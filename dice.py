"""A script to calculate dice, hausdorff and other metrict of dicom structures
using plastimatch, rCT isodose vs WEPL isodose"""

import subprocess

from field_specific_wepl import test_ct_base_dir, gantry_angles, levels, voi_name
from registrations import rotations_translations_by_patient_and_rCT

from dice_2 import get_rs_filename, get_rs_file_from_dir, rsfile_to_mha_txt
from dice_2 import strip_all_s_except, call_dice_calc

def apply_rigid(filename, rot, trn, like_img):
    """Apply rigid transformation to filename with output in the same size and res as like_img"""
    output_filename = filename + "_moved"
    subprocess.run(["/home/andreas/Projects/build-cbct/gcc9-Rel/bin/apply_affine",
                    filename + ".mha", output_filename + ".mha",
                    trn, rot, like_img])
    return output_filename

def dice_calc(ref_ct_dir, test_ct_dir,
              gantry_angle_deg, couch_angle_deg,
              s_voi_name, rot, trn):
    """Extract the appropriate voi from the dicom RS files and calculate dice++ between them"""
    test_name = test_ct_dir.split('/')[-1]

    ## WEPL structure
    ref_rs_file = get_rs_filename(ref_ct_dir.split('/')[-2], s_voi_name, test_name,
                                  gantry_angle_deg, couch_angle_deg)
    # ref_rs_file = get_RS_file_from_dir(ref_ct_dir)

    # Generate structure images from RS dicom
    ref_structname = rsfile_to_mha_txt(ref_rs_file, "pCT", ref_ct_dir)
    # Strip all the structures except the one we care about from the generated images
    stripped_ref_name = strip_all_s_except("WEPL" + s_voi_name, ref_structname)


    ## rCT isodose structure
    test_rs_file = get_rs_file_from_dir(test_ct_dir)
    # Generate structure images from RS dicom
    test_structname = rsfile_to_mha_txt(test_rs_file, test_name, test_ct_dir)
    # Strip all the structures except the one we care about from the generated images
    test_voi_name = s_voi_name.replace("pCT", test_name)
    stripped_test_name = strip_all_s_except(test_voi_name, test_structname)

    # Apply rigid registration to stripped structure image
    moved_stripped_test_name = apply_rigid(stripped_test_name, rot, trn, stripped_ref_name + ".mha")

    # Calculate dice
    output_filename = ("all_rCT_" + ref_ct_dir.split('/')[-2] + "_" +
                       ".".join(ref_rs_file.split('.')[1:-1]) + ".txt")
    file_p = open(output_filename, 'w')
    call_dice_calc(stripped_ref_name, moved_stripped_test_name, file_p)
    return output_filename

def main():
    """Loop over all patients, rCTs, angles and dose levels and calculate dice++"""
    for patient, rcts in rotations_translations_by_patient_and_rCT.items():

        cur_patient_dir = test_ct_base_dir + patient + "/"
        for rct, rot_trans in rcts.items():

            rot = "%f,%f,%f" % (rot_trans[0], rot_trans[1], rot_trans[2])
            trans = "%f,%f,%f" % (rot_trans[3], rot_trans[4], rot_trans[5])
            cur_ref_ct_dir = cur_patient_dir + "pCT"
            cur_test_ct_dir = cur_patient_dir + rct
            for index_a, angle in enumerate(gantry_angles):
                for level in levels:
                    if (index_a in [0, 1] and level in levels[:3]):
                        voi = voi_name(level, angle)
                        output = dice_calc(cur_ref_ct_dir, cur_test_ct_dir,
                                           angle, 0, voi, rot, trans)
                        print(output)
                    if (index_a in [2, 3] and level in levels[2:]):
                        voi = voi_name(level, angle)
                        output = dice_calc(cur_ref_ct_dir, cur_test_ct_dir,
                                           angle, 0, voi, rot, trans)
                        print(output)


if __name__ == "__main__":
    main()
