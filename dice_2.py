"""A script to calculate dice, hausdorff and other metrict of dicom structures
using plastimatch, pCT vs WEPL isodose"""

import subprocess
import os

from field_specific_wepl import plastimatch_exe, test_ct_base_dir, gantry_angles, levels, voi_name
from registrations import rotations_translations_by_patient_and_rCT

# ref_rs_file = ref_ct_dir + "RS_00158.dcm"
def get_rs_filename(patient, s_voi_name, ct_name, gantry, couch):
    """Generates the RS dicom filename from the voi name, ct name, and angles"""
    return ("RS.wepl_structure_" + patient + "_" + s_voi_name + "_" + ct_name
            + "_G" + str(gantry) + "_C" + str(couch) + ".dcm")

def get_rs_file_from_dir(ct_dir):
    """Loops over all files in dir and returns the first with RS in the name"""
    for filename in os.listdir(ct_dir):
        if "RS" in filename:
            return ct_dir + '/' + filename
    print("NO RS FILE FOUND FOR: " + ct_dir + " THIS WILL FAIL!")
    return ""

def rsfile_to_mha_txt(rs_file, name, ct_dir):
    """Generates a structure image in mha with a list of the structures"""
    struct_name = "structs_" + name
    subprocess.run([plastimatch_exe,
                    "convert", "--input", rs_file,
                    "--output-ss-img", struct_name + ".mha",
                    "--output-ss-list", struct_name + ".txt",
                    "--referenced-ct", ct_dir])
    return struct_name

def mask_struct_img(filename):
    """Remove all structures except the one in filename_stripped.txt"""
    out_name = filename + "_stripped"
    subprocess.run([plastimatch_exe, "convert",
                    "--input-ss-img", filename + ".mha",
                    "--input-ss-list", out_name + ".txt",
                    "--output-labelmap", out_name + ".mha"])
    return out_name

def strip_all_s_except(voi, filename):
    """Stripped all lines except the one containing voi from filename.txt"""
    file_p = open(filename + ".txt", 'r')
    lines = file_p.readlines()
    file_p.close()
    file_p_new = open(filename + "_stripped.txt", 'w')
    for line in lines:
        if voi in line:
            file_p_new.write(line)
    file_p_new.close()

    # Remove all other structures from image struct
    return mask_struct_img(filename)

def call_dice_calc(ref_name, test_name, out_file_p):
    """Calculate dice, hausdorff and such with plastimatch"""
    subprocess.Popen([plastimatch_exe, "dice", "--all",
                      ref_name + ".mha", test_name + ".mha"],
                     stdout=out_file_p)

def dice_calc(ref_ct_dir, test_ct_dir,
              gantry_angle_deg, couch_angle_deg,
              s_voi_name):
    """Extract the appropriate voi from the dicom RS files and calculate dice++ between them"""
    test_name = test_ct_dir.split('/')[-1]

    ## WEPL structure
    ref_rs_file = get_rs_filename(ref_ct_dir.split('/')[-2], s_voi_name, test_name,
                                  gantry_angle_deg, couch_angle_deg)
    # Generate structure images from RS dicom
    ref_structname = rsfile_to_mha_txt(ref_rs_file, "WEPL", ref_ct_dir)
    # Strip all the structures except the one we care about from the generated images
    stripped_ref_name = strip_all_s_except("WEPL" + s_voi_name, ref_structname)

    ## pCT isodose structure
    test_rs_file = get_rs_file_from_dir(ref_ct_dir)
    # Generate structure images from RS dicom         #test_name
    test_structname = rsfile_to_mha_txt(test_rs_file, "pCT", ref_ct_dir)
    # Strip all the structures except the one we care about from the generated images
    stripped_test_name = strip_all_s_except(s_voi_name, test_structname)

    # Calculate dice
    output_filename = ("all_" + ref_ct_dir.split('/')[-2] + "_" +
                       ".".join(ref_rs_file.split('.')[1:-1]) + ".txt")

    file_p = open(output_filename, 'w')
    call_dice_calc(stripped_ref_name, stripped_test_name, file_p)
    return output_filename

def main():
    """Loop over all patients, rCTs, angles and dose levels and calculate dice++"""
    for patient, rcts in rotations_translations_by_patient_and_rCT.items():
        cur_patient_dir = test_ct_base_dir + patient + "/"
        for rct, _ in rcts.items():
            cur_ref_ct_dir = cur_patient_dir + "pCT"
            cur_test_ct_dir = cur_patient_dir + rct
            for index_a, angle in enumerate(gantry_angles):
                for level in levels:
                    if (index_a in [0, 1] and level in levels[:3]):
                        voi = voi_name(level, angle)
                        output = dice_calc(cur_ref_ct_dir, cur_test_ct_dir,
                                           angle, 0, voi)
                        print(output)
                    if (index_a in [2, 3] and level in levels[2:]):
                        voi = voi_name(level, angle)
                        output = dice_calc(cur_ref_ct_dir, cur_test_ct_dir,
                                           angle, 0, voi)
                        print(output)


if __name__ == "__main__":
    main()
