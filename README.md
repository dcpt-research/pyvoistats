### Python scripts to calculate statistics for DICOM structures

 * Mask by VOI
   - Overrides CT values inside the structure that matches the given string.
   - Using [PyTRIP](https://github.com/pytrip/pytrip)
 * Field specific WEPL
   - Computes the equivalent WEPL to a structure in another image.
   - Using [CbctRecon](https://gitlab.com/agravgaard/cbctrecon)
 * Dice #
   - Calculates DICE and hausdorff between structures.
   - Using [plastimatch](https://gitlab.com/plastimatch/plastimatch)
 * My Regex #
   - Data extraction using regular expressions on the output from the [plastimatch](https://gitlab.com/plastimatch/plastimatch) dice function.
 * Run
   - A short bash script to run the three scripts above in the right order.
