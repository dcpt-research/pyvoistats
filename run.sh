#!/bin/sh


## Chain commands, so we only continue on success
mkdir -p results_ro && cd results_ro \
    && sed -i 's/PTV/RO/g' ../field_specific_wepl.py \
    && python ../field_specific_wepl.py \
    && echo "WEPL CALCULATION FINISHED" \
    && python ../dice.py \
    && echo "DICE CALCULATION FINISHED" \
    && python ../dice_2.py \
    && echo "DICE_2 CALCULATION FINISHED" \
    && python ../dice_3.py \
    && echo "ALL DICE CALCULATED" \
    && sleep 1 \
    && python ../my_regex.py \
    && python ../my_regex_2.py \
    && python ../my_regex_3.py \
    && echo "REGEX FINISHED!"


## Chain commands, so we only continue on success
cd .. && mkdir -p results_ptv && cd results_ptv \
    && sed -i 's/RO/PTV/g' ../field_specific_wepl.py \
    && python ../field_specific_wepl.py \
    && echo "WEPL CALCULATION FINISHED" \
    && python ../dice.py \
    && echo "DICE CALCULATION FINISHED" \
    && python ../dice_2.py \
    && echo "DICE_2 CALCULATION FINISHED" \
    && python ../dice_3.py \
    && echo "ALL DICE CALCULATED" \
    && sleep 1 \
    && python ../my_regex.py \
    && python ../my_regex_2.py \
    && python ../my_regex_3.py \
    && echo "REGEX FINISHED!"

echo "Copying results to USB..."
cp -R results_*/dice* /run/media/andreas/KINGSTON2/Kia_20200113/
