"""Script to override values in dicom images by structure"""
import os.path
import subprocess
import glob

import pytrip as pt

def main():
    """Main function"""
    pt_nums_hu_val = {
        (1, 4),
        (2, 13),
        (3, 7),
        (4, 8),
        (8, 8),
        (9, 14),
        (12, 4)
        }
    for pt_num, hu_val in pt_nums_hu_val:
        ct_dir = "HUH%d/pCT" % pt_num
        print(ct_dir)

        if not os.path.isdir(ct_dir + "_unmasked"):
            subprocess.run(["mv", ct_dir, ct_dir + "_unmasked"])
        elif os.path.isdir(ct_dir) and os.listdir(ct_dir):
            # unmasked exists and ct_dir is not empty
            # Copy the RS file, just to be safe, then skip the rest
            rs_file = glob.glob(ct_dir + "_unmasked/RS*")
            subprocess.run(["cp", rs_file[0], ct_dir])
            continue

        dh_data = pt.read_dicom_dir(ct_dir + "_unmasked")

        ctx_in = pt.CtxCube()
        ctx_in.read_dicom(dh_data)

        vdx_in = pt.VdxCube(ctx_in)
        vdx_in.read_dicom(dh_data)

        preset = -1
        for cur_voi in vdx_in.vois:
            print(cur_voi.name)
            if ("vesica urinaria" in cur_voi.name.lower()
                    or "vesica lb" in cur_voi.name.lower()
                    or "vesicaunik2" in cur_voi.name.lower()):
                preset = hu_val
                vcube = cur_voi.get_voi_cube()
                mask = (vcube.cube == 1000)
                print(preset)
                ctx_in.cube[mask] = preset
        if preset == -1:
            print("No bladder found for patient %d !!!" % pt_num)
            return

        ctx_in.write_dicom(ct_dir)

        rs_file = glob.glob(ct_dir + "_unmasked/RS*")
        subprocess.run(["cp", rs_file[0], ct_dir])

if __name__ == "__main__":
    main()
