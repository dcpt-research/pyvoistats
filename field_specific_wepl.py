import os.path
import subprocess

from registrations import rotations_translations_by_patient_and_rCT

executable = "/home/andreas/Projects/build-cbct/gcc9-Rel/bin/bigart"
plastimatch_exe = "/home/andreas/Projects/plm-build-gcc/plastimatch"

test_ct_base_dir = "/home/andreas/SecureDrive2/Kia_20191210/added_iso/PTV/"

def voi_name(level, angle):
    """Generate VOI name from dose level and angle"""
    return "pCT_D%d[%%]_f%d" %(level, angle)

gantry_angles = [270.0, 90.0, 35.0, 325.0]
levels = [25, 35, 45, 65, 55]

def rename_out_file(voi, rep_ct_dir, gantry_angle):
    """Generate the output filename from input params (doesn't match)"""
    better_voi_name = voi.replace(" ", "_").replace("/", "-")
    in_str = "RS.wepl_structure_%s_%s_G%d.0_C0.dcm" % (better_voi_name,
                                                       rep_ct_dir.split('/')[-1],
                                                       gantry_angle)

    out_str = "RS.wepl_structure_%s_%s_%s_G%d.0_C0.dcm" % (rep_ct_dir.split('/')[-2],
                                                           better_voi_name,
                                                           rep_ct_dir.split('/')[-1],
                                                           gantry_angle)
    subprocess.run(["mv", in_str, out_str])
    return out_str

def call_wepl_calc(ref_ct_dir, test_ct_dir,
                   gantry_angle_deg, couch_angle_deg,
                   s_voi_name, translation, rotation):
    """Convert dcm to mha if necessary, then calculate WEPL structure"""
    if not os.path.isfile("%s/recalc_in.mha" % test_ct_dir):
        subprocess.run([plastimatch_exe,
                        "convert", "--input", test_ct_dir,
                        "--output-img", "%s/recalc_in.mha" % (test_ct_dir)
                        ])
    subprocess.run([executable,
                    ref_ct_dir,
                    test_ct_dir, s_voi_name,
                    str(gantry_angle_deg),
                    str(couch_angle_deg),
                    translation, rotation])
    return rename_out_file(s_voi_name, test_ct_dir, gantry_angle_deg)

def main():
    """Loop over all patients, rCTs, angles and dose levels to calculate WEPL structures"""
    out_files = []
    for patient, rcts in rotations_translations_by_patient_and_rCT.items():

        cur_patient_dir = test_ct_base_dir + patient + "/"
        cur_ref_ct_dir = cur_patient_dir + "pCT"
        for rct, rot_trans in rcts.items():

            rot = "%f,%f,%f" % (rot_trans[0], rot_trans[1], rot_trans[2])
            trans = "%f,%f,%f" % (rot_trans[3], rot_trans[4], rot_trans[5])
            cur_test_ct_dir = cur_patient_dir + rct
            for index_a, angle in enumerate(gantry_angles):
                for level in levels:
                    if (index_a in [0, 1] and level in levels[:3]):
                        voi = voi_name(level, angle)
                        out_files.append(call_wepl_calc(cur_ref_ct_dir,
                                                        cur_test_ct_dir,
                                                        angle, 0, voi,
                                                        trans, rot))
                    if (index_a in [2, 3] and level in levels[2:]):
                        voi = voi_name(level, angle)
                        out_files.append(call_wepl_calc(cur_ref_ct_dir,
                                                        cur_test_ct_dir,
                                                        angle, 0, voi,
                                                        trans, rot))
                # end for level
            # end for angle
        # end for rCT
    # end patients

    print("Done!")


if __name__ == "__main__":
    main()
