# coding=utf8
"""Script to get DICE and Hausdorff data from plastimatch output"""

import re
from subprocess import check_output

from registrations import rotations_translations_by_patient_and_rCT

def main():
    """Main"""
    # regex = r"(?sm)D(\d\d)\[\%\]_f(\d+)_rCT(\d+).*?^DICE:\s+(\d+\.\d+).*?^Hausdorff distance = (\d+\.\d+)$"

    regex = r"(?sm)D(\d\d)\[\%\]_f(\d+)_rCT(\d+).*?^DICE:\s+(\d+\.\d+).*?^Percent \(0\.95\) Hausdorff distance \(boundary\) = (\d+\.\d+)$"

    for patient, _ in rotations_translations_by_patient_and_rCT.items():
        test_str = check_output("tail -n +1 all_rCT_" + patient + "_wepl*", shell=True)

        match_all = re.findall(regex, test_str.decode("utf-8"))
        file_p = open("dice_hausdorff_" + patient + "_rCTisodose_vs_WEPL.txt", 'w')
        file_p.write("Dose;Gantry;rCT;DICE;Hausdorff\n")
        for matches in match_all:
            print(matches)
            file_p.write("{};{};{};{};{}\n".format(
                matches[0], matches[1], matches[2], matches[3], matches[4]))
        file_p.close()

if __name__ == "__main__":
    main()
